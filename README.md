## What is this?
This repo is used to transfer learning a MaskRCNN model. This repo is slightly modified from the original [maskrcnn-benchmark repo](https://github.com/facebookresearch/maskrcnn-benchmark)


## Dependencies
1. Anaconda3
2. CUDA 10.0
3. CUDNN 7.6.3
4. pytorch 1.4
5. maskrcnn-benchmark


## Installation
```installation

git clone https://gitlab.com/kennyvoo/maskrcnn-benchmark.git
cd maskrcnn-benchmark

# create environment
conda env create --file mask_train_env.yml
conda activate mask_train

# build the maskrcnn 
python setup.py build develop

# install apex
git clone https://github.com/NVIDIA/apex.git 
cd apex
python setup.py install --cuda_ext --cpp_ext ( if there's error,try this.  python setup.py install)


# install pycocotools
git clone https://github.com/cocodataset/cocoapi.git
cd cocoapi/PythonAPI
python setup.py build_ext install

# install cityscapesScripts
git clone https://github.com/mcordts/cityscapesScripts.git
cd cityscapesScripts/
python setup.py build_ext install


```

## Prepare pretrained weight for transfer learning
1. Download the [pretrained model](https://download.pytorch.org/models/maskrcnn/e2e_mask_rcnn_R_50_FPN_1x.pth)
2. Move it into weights folder in maskrcnn-benchmark
3. Run trim_model.py to remove last layers,optimizers,scheduler and iteration entries.
>python trim_model.py

4. **e2e_mask_rcnn_R_50_FPN_1x_trimmed.pth** will be created. 



## Prepare your custom coco dataset
1. Follow the instruction to [prepare your own dataset](https://gitlab.com/kennyvoo/custom_coco_dataset)
2. create and move your train/val dataset into datasets/custom_dataset. It should have the following structure.
```txt
datasets
|_custom_dataset		
   |_train_dataset
	|_JPEGImages [Contains all the images for training]
	|_annotations.json
   |_val_dataset
	|_JPEGImages [Contains all the images for training]
	|_annotations.json
```


## Custom config for transfer learning
1. Change the followings line in configs/custom/e2e_mask_rcnn_R_50_FPN_1x_caffe2.yaml.
```txt
    NUM_CLASSES: 4		#Change to your number of classes +2
    OUTPUT_DIR: "./weights/custom/"    # saved weight output directory
```
2. The default parameters will train the model for 3000 iterations, the learning rate will decrease every 500 iterations from 1000 iterations. Validation will be run every 200 iterations and the model will be saved every 200 iterations. You can change other parameters if the default one doesn't work for you. For the NUM_CLASSES, instead of +1, I added +2, because it doesn't work for my case. The training does not converge.

## Start traininig
1. run the following code

>python tools/train_net.py --config-file configs/custom/e2e_mask_rcnn_R_50_FPN_1x_caffe2.yaml


## Convert the model to onnx
1. Follow the instruction in this [repo](https://gitlab.com/kennyvoo/maskrcnn_onnx/-/tree/onnx_stage_mrcnn) to convert the model to onnx model.
